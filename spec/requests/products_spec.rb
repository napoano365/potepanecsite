require 'rails_helper'

RSpec.describe "Products_resquest", type: :request do
  describe "GET #show" do
    let!(:product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "renders the show template" do
      expect(response).to render_template(:show)
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end
  end
end
